from django.db import models

# Create your models here.

class fields(models.Model):
    field = models.CharField(max_length = 30)
    author = models.CharField(max_length = 30)
    location = models.CharField(max_length = 52)
    date_created = models.DateField()
    date_modified = models.DateField()
    type = models.CharField(max_length = 30)
