from django.apps import AppConfig


class ConnectToDyanamoConfig(AppConfig):
    name = 'connect_to_dyanamo'
